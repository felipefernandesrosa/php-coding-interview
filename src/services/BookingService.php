<?php

namespace Src\services;

use Src\models\BookingModel;
use Src\models\ClientModel;
use Src\models\DogModel;

class BookingService {

    private function getBookingModel(): BookingModel
    {
        return new BookingModel();
    }
    public function createBooking($clientId, $attributes)
    {
        $clientDogs = (new DogModel())->getDogs();
        $clientAgeDogs = 0;
        $clientDog = array_filter($clientDogs, function ($item) use ($clientId) {
            return $item['clientid'] === $clientId;
        });

        foreach ($clientDog as $dog) {
            $clientAgeDogs += $dog['age'];
        }

        $averageAge = round($clientAgeDogs / count($clientDog));

        if($averageAge < 10) {
            $attributes['price'] = $attributes['price'] - ($attributes['price'] * 0.10);
        }
        return $this->getBookingModel()->createBooking(['clientid' => $clientId, ...$attributes]);
    }

}