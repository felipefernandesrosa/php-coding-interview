<?php

namespace Src\rule;

use Rakit\Validation\Rule;
use Src\helpers\NumVerifyAPI;

class ValidatePhone extends Rule
{
    public function check($value): bool
    {
        $validatePhone = NumVerifyAPI::validatePhone($value);
        if($validatePhone->valid === false) {
            return false;
        }
        return true;
    }
}