<?php

namespace Src\controllers;

use Src\helpers\Helpers;
use Src\models\ClientModel;
use Src\requests\Client\CreateClientRequest;

class ClientController {

	private function getClientModel(): ClientModel {
		return new ClientModel();
	}

	public function getClients() {
		return $this->getClientModel()->getClients();
	}

	public function createClient(CreateClientRequest $client)
    {
		return $this->getClientModel()->createClient($client->validate());
	}

	public function updateClient($client): array
    {
		return $this->getClientModel()->updateClient($client);
	}

	public function getClientById($id) {
		return $this->getClientModel()->getClientById($id);
	}
}