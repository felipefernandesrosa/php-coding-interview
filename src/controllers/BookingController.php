<?php

namespace Src\controllers;

use Src\models\BookingModel;
use Src\models\ClientModel;
use Src\requests\Booking\CreateBookingRequest;
use Src\requests\Request;
use Src\services\BookingService;

class BookingController {

    protected BookingService $bookingService;
    public function __construct()
    {
        $this->bookingService = new BookingService();
    }
	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

    public function createBooking($clientId, CreateBookingRequest $request)
    {
        return $this->bookingService->createBooking($clientId, $request->validate());
    }
}