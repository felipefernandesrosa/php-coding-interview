<?php
namespace Src\requests\Booking;
use Src\requests\Request;

class CreateBookingRequest extends Request
{
    public function __construct(protected $method, protected array $request) {
        parent::__construct();
    }

    public function rules() : array
    {
        return [
            'price' => 'required',
            'checkindate' => 'required',
            'checkoutdate' => 'required',
        ];
    }
}