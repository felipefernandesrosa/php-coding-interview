<?php
namespace Src\requests;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;
use Src\rule\ValidatePhone;

class Request
{
    protected Validator $validator;
    protected array $request = [], $rules = [];
    public function __construct()
    {
        $this->validator = new Validator;
        $this->validator->addValidator('phone', new ValidatePhone());
    }

    public function rules() : array
    {
        return $this->rules;
    }

    public function validate() : \Rakit\Validation\ErrorBag|array
    {
        $validation = $this->validator->validate($this->request, $this->rules());

        if ($validation->fails()) {
            return $validation->errors();
        }

        return $this->request;
    }
}