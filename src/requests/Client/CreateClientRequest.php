<?php
namespace Src\requests\Client;
use Src\requests\Request;

class CreateClientRequest extends Request
{
    public function __construct(protected $method, protected array $request) {
        parent::__construct();
    }

    public function rules() : array
    {
        return [
            'username' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|phone'
        ];
    }
}