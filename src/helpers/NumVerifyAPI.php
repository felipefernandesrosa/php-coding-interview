<?php

namespace Src\helpers;

use GuzzleHttp\Client;

class NumVerifyAPI
{
    private static ?NumVerifyAPI $instance = null;

    private function __construct() {

    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new NumVerifyAPI();
        }
        return self::$instance;
    }

    public static function validatePhone($phone)
    {
        try {
            $response = (new Client())->request('GET', "http://apilayer.net/api/validate?access_key=324fce71eb8097b5669fb85880f76b08&number={$phone}");
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}