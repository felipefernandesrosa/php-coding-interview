<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\ClientController;
use Src\requests\Client\CreateBookingRequest;

class ClientControllerTest extends TestCase {

	private $client;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->client = new ClientController();
	}

	/** @test */
	public function getClients() {
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals(1, $results[0]['id']);
		$this->assertEquals('arojas', $results[0]['username']);
		$this->assertEquals('Antonio Rojas', $results[0]['name']);
		$this->assertEquals('arojas@dogeplace.com', $results[0]['email']);
		$this->assertEquals('1234567', $results[0]['phone']);
	}

    /** @test */
	public function createClient() {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'newuser@dogeplace.com',
			'phone' => '+14844731077'
		];

		$this->client->createClient(new CreateBookingRequest($_POST, $client));
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}

	public function updateClient() {
		$client = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'cperez@dogeplace.com',
			'phone' => '2222222'
		];


		$this->client->updateClient($client);
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}
}