<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\BookingController;
use Src\requests\Booking\CreateBookingRequest;

class BookingControllerTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new BookingController();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals(1, $results[0]['id']);
		$this->assertEquals(1, $results[0]['clientid']);
		$this->assertEquals(200, $results[0]['price']);
		$this->assertEquals('2021-08-04 15:00:00', $results[0]['checkindate']);
		$this->assertEquals('2021-08-11 15:00:00', $results[0]['checkoutdate']);
	}

    /** @test */
    public function createBooking() {

        $results = $this->booking->createBooking(3, new CreateBookingRequest($_POST,
            [
                "price" => 100,
                "checkindate" => "2021-08-05 15:00:00",
                "checkoutdate" => "2021-08-08 15:00:00"
            ]
        ));

        $this->assertIsArray($results);
        $this->assertIsNotObject($results);

        $this->assertEquals(3, $results['clientid']);
        $this->assertEquals(100, $results['price']);
        $this->assertEquals("2021-08-05 15:00:00", $results['checkindate']);
        $this->assertEquals("2021-08-08 15:00:00", $results['checkoutdate']);
    }

    public function createBookingWithDiscount() {

        $results = $this->booking->createBooking(1, new CreateBookingRequest($_POST,
            [
                "price" => 100,
                "checkindate" => "2021-08-05 15:00:00",
                "checkoutdate" => "2021-08-08 15:00:00"
            ]
        ));

        $this->assertIsArray($results);
        $this->assertIsNotObject($results);

        $this->assertEquals(1, $results['clientid']);
        $this->assertEquals(90, $results['price']);
    }
}